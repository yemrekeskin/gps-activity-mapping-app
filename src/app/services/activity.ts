import { IActivity } from '../shared/activity.model';

export const SavedActivities: IActivity[]  = [
  {
    "id": 1,
    "name":"Main Bike Trails",
    "date": new Date('06/05/1990'),
    "distance": 6.2,
    "comments": "Nice day, cool temps",
    "gpxData": "../../assets/gpx/3525849.gpx"
  },
  {
    "id": 2,
    "name":"Industrial Park",
    "date": new Date('06/05/1990'),
    "distance": 7.5,
    "comments": "Nice day, cool temps",
    "gpxData": "../../assets/gpx/3552718.gpx"
  }
]
