import { SavedActivities } from './activity';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';

var apiToken = environment.mapbox_api_key;
declare var omnivore: any;
declare var L: any;

const defaultCoords: number[] = [40, -80];
const defaultZoom: number = 8;

@Injectable({
  providedIn: 'root',
})
export class MapService {
  constructor() {}

  getActivity(id: number) {
    return SavedActivities.slice(0).find((d) => d.id === id);
  }

  plotActivity(id: number) {

    var map = L.map('map').setView(defaultCoords, defaultZoom);

    map.maxZoom = 100;

    L.tileLayer(
      'https://api.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}',
      {
        attribution:
          'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox.satellite',
        accessToken: apiToken,
      }
    ).addTo(map);

    var myStyle = {
      color: '#3949AB',
      weight: 5,
      opacity: 0.95,
    };

    var customLayer = L.geoJson(null, {
      style: myStyle,
    });

    let gpxData = this.getActivity(id).gpxData;
    var gpxLayer = omnivore
      .gpx(gpxData, null, customLayer)
      .on('ready', function () {
        map.fitBounds(gpxLayer.getBounds());
      })
      .addTo(map);
  }
}
