import { IActivity } from './../shared/activity.model';
import { Injectable } from '@angular/core';
import { SavedActivities } from './activity';

@Injectable({
  providedIn: 'root'
})
export class ActivityService {

  constructor() { }

  getActivities(): IActivity[] {
    return SavedActivities.slice(0);
  }

  getTotalActivities(allActivities: IActivity[]): number {
    return allActivities.length;
  }

  getTotalDistance(allActivities: IActivity[]): number {
    let totalDistance = 0;
    allActivities.forEach((item) => {
      totalDistance += item.distance;
    })
    return totalDistance;
  }

  getFirstDate(allActivities: IActivity[]): Date {
    let earliestDate = new Date('01/01/9999');
    allActivities.forEach((item) => {
      let currentDate = item.date;
      if(currentDate < earliestDate)
        earliestDate = currentDate;
    })
    return earliestDate;
  }

}
