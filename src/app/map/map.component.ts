import { IActivity } from './../shared/activity.model';
import { MapService } from './../services/map.service';
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, AfterViewInit {


  selectedActivity: IActivity;

  constructor(
    private _route: ActivatedRoute,
    private _mapService: MapService
  ) {

  }

  ngOnInit(): void {
    let id = +this._route.snapshot.params['id'];
    this.selectedActivity = this._mapService.getActivity(id);
  }

  ngAfterViewInit() {
    let id = +this._route.snapshot.params['id'];
    this._mapService.plotActivity(id);
  }

}
